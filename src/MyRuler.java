import java.awt.Point;

import com.ibm.ruler.ICastle;
import com.ibm.ruler.IKnight;
import com.ibm.ruler.IObject;
import com.ibm.ruler.IPeasant;
import com.ibm.ruler.Ruler;
import com.ibm.ruler.World;
/**
 * My CodeRuler Code (in progress)
 * 
 * Notes:
 * 
 * For knights: split-up for loop so attacking
 * can fully prioritise moving
 * 
 * @author Jamie Langille
 * @version Date: 10/30/10
 */
public class MyRuler extends Ruler {

	/**
	 *  start x and y positions for early game peasant movement
	 */
	private int startx, starty;
	private boolean onlyPeasants;

	/**
	 *  find starting x and y positions of my first castle
	 */
	public void initialize() {
		startx = this.getCastles()[0].getX();
		starty = this.getCastles()[0].getY();
		onlyPeasants = false;
		
//		for(IRuler person : World.getOtherRulers())
//			System.out.println(person.getPeasants()[0].getId());
		
		/*		lower IDs move first: peasants are always
		 *		less than knights, and values are evenly distributed between players
		 *		i.e. every nth number for n players
		 *		
		 *		players are randomly shuffled-later players in
		 *		the list have lower ID numbers
		 */
		//		for(IPeasant p : this.getPeasants())
		//			System.out.println(p.getId());
		//		for(IKnight k : this.getKnights())
		//			System.out.println(k.getId());
		//		
		//		IRuler enemy = World.getOtherRulers()[0];
		//		for(IPeasant p : enemy.getPeasants())
		//			System.out.println(p.getId());
		//		for(IKnight k : enemy.getKnights())
		//			System.out.println(k.getId());
	}

	/**
	 * ensures that a direction is within the range 1<=x<=8
	 * i.e. d=10; d=range(10); d=2.
	 * @param dir	proposed direction
	 * @return		proper direction to move in
	 */
	private int range(int dir) {
		if (dir > 8)
			return dir-8;
		if (dir < 1)
			return dir + 8;
		return dir;
	}

	/**
	 * derpderpderp
	 */
	public void orderSubjects(int lastMoveTime) {

		/*
		 * Peasant algorithm
		 * spiral around my castle at beginning
		 * fill land in areas (fill, not random)
		 * random otherwise
		 */
		P: for (IPeasant p : this.getPeasants()) {

			// initial direction around castle
			int dir = p.getDirectionTo(startx, starty) - 2;
			for (int d = dir, c = 0; c < 8; c++, d--) {
				d = range(d);
				Point pt = World.getPositionAfterMove(p.getX(), p.getY(), d);

				// off-screen=next direction
				if (pt == null)
					continue;

				// does not own this square of land
				if (World.getLandOwner(pt.x, pt.y) == null
						|| !World.getLandOwner(pt.x, pt.y).equals(this)) {

					// someone is there
					if (World.getObjectAt(pt.x, pt.y) != null){

						// dodge knights and ignore peasants
						if (World.getObjectAt(pt.x, pt.y) instanceof IKnight){
							move(p, range(d-4));
							continue P;
						}
						else
							continue;
					}

					// no one is there-take it
					else {
						move(p, d);
						continue P;
					}
				}
			}

			// randomly move if all else fails
			move(p, rand());
		}

	/*
	 * Knight algorithm
	 */

	// find the leading ruler who has units left
	/*IRuler topruler = null;
	for (IRuler ruler : World.getOtherRulers()) {
		if (ruler.getPeasants().length+ruler.getKnights().length > 0)
			if (topruler == null || topruler.getPoints() < ruler.getPoints())
				topruler = ruler;
	}*/

	K: for (IKnight k : this.getKnights()) {
		
		// default direction: away from centre to defend castles if they get captured
		int dir = rand();
		int dist = Integer.MAX_VALUE;

//		if (topruler != null){

			// find the closest of these castles to move to
			if (dist == Integer.MAX_VALUE){
				for (ICastle c : World.getOtherCastles())//topruler.getCastles()
					if (c.getDistanceTo(k.getX(), k.getY()) < dist){
						dist = c.getDistanceTo(k.getX(), k.getY());
						dir = k.getDirectionTo(c.getX(), c.getY());
					}
			}
			
			// find closest distance to an enemy's knights
			if (dist == Integer.MAX_VALUE){
				for (IKnight enemy : World.getOtherKnights())//topruler.getKnights())
					if (enemy.getDistanceTo(k.getX(), k.getY()) < dist){
						dist = enemy.getDistanceTo(k.getX(), k.getY());
						dir = k.getDirectionTo(enemy.getX(), enemy.getY());
					}
			}

			// find closest distance to an enemy's peasants
			if (dist == Integer.MAX_VALUE){
				for (IPeasant enemy : World.getOtherPeasants())//topruler.getPeasants()){
					if (enemy.getDistanceTo(k.getX(), k.getY()) < dist){
						dist = enemy.getDistanceTo(k.getX(), k.getY());
						dir = k.getDirectionTo(enemy.getX(), enemy.getY());
					}
				onlyPeasants = true;
			}

//		}


		// Enemy Peasant/Castle/Knight is there=capture
		for (int d = dir, c = 0; c < 8; c++, d--) {
			d = range(d);
			Point pt = World.getPositionAfterMove(k.getX(), k.getY(), d);
			if (pt != null){
				IObject obj = World.getObjectAt(pt.x, pt.y);
				if (obj != null && !obj.getRuler().equals(this)) {

					// dodge knights (more powerful)
					if (obj instanceof IKnight){
//						IKnight bk = (IKnight)obj;

						//smarter-higher ids go first
//						if(k.getId() > bk.getId() || bk.getStrength() > k.getStrength()){
							capture(k, d);
							continue;// K;
//						}
//						else{
//							move(k, range(d-4));
//							continue;// K;
//						}
					}
				}  else if (!onlyPeasants && obj != null && obj instanceof ICastle && d % 2 == 1){
					move(k, 0);
					continue;// K;
				}
			}
		}
		
		// capture other
		for (int d = dir, c = 0; c < 8; c++, d--) {
			d = range(d);
			Point pt = World.getPositionAfterMove(k.getX(), k.getY(), d);
			if (pt != null){
				IObject obj = World.getObjectAt(pt.x, pt.y);
				if (obj != null && !obj.getRuler().equals(this)) {
					capture(k, d);
					continue K;
				}
			}
		}

		for (int d = dir, c = 0; c < 8; c++, d--) {
			d = range(d);
			Point pt = World.getPositionAfterMove(k.getX(), k.getY(), d);

			// off screen=next dir
			if (pt == null)
				continue;

			// no one is there=move
			if (World.getObjectAt(pt.x, pt.y) == null) {
				move(k, d);
				continue K;
			}

		}

		// default move if all else fails (only if I have all the castles-a good sign)
		move(k, dir);
	}

	/*
	 * Castle algorithm
	 * maintain a 1:2 ratio of k:p
	 */
	if (onlyPeasants || this.getKnights().length*2 > this.getPeasants().length)
		for (ICastle c : this.getCastles())
			this.createPeasants(c);
	else
		for (ICastle c : this.getCastles())
			this.createKnights(c);
	}

	/**
	 * random direction
	 * @return	an int from 1 to 8 inclusive
	 */
	private int rand() {
		return (int) (Math.random() * 8) + 1;
	}
}