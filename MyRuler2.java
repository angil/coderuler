import java.awt.Point;

import com.ibm.jc.JavaChallenge;
import com.ibm.ruler.ICastle;
import com.ibm.ruler.IKnight;
import com.ibm.ruler.IObject;
import com.ibm.ruler.IPeasant;
import com.ibm.ruler.IRuler;
import com.ibm.ruler.Ruler;
import com.ibm.ruler.World;

/**
 * CodeRuler Code for Richmond Hill High School's Computer Science Club
 * 
 * @author Kevin Choi && Jamie Langille
 * @version Date: 11/02/10;11:41am
 */
@JavaChallenge(name = "aDeadParrot", organization = "Richmond Hill CSC")
public class MyRuler extends Ruler {

	/**
	 * start x and y positions for early game peasant movement
	 */
	private int startx, starty;
	private boolean onlyPeasants, nothin;

	/**
	 * find starting x and y positions of my first castle
	 */
	public void initialize() {
		startx = this.getCastles()[0].getX();
		starty = this.getCastles()[0].getY();
		onlyPeasants = false;
		nothin = false;
	}

	/**
	 * ensures that a direction is within the range 1<=x<=6 i.e. d=10;
	 * d=range(8); d=2.
	 * 
	 * @param dir
	 *            proposed direction
	 * @return proper direction to move in
	 */
	private int range(int dir) {
		if (dir > 6)
			return dir - 6;
		if (dir < 1)
			return dir + 6;
		return dir;
	}

	/**
	 * derpderpderp
	 */
	public void orderSubjects(int lastMoveTime) {

		/*
		 * Peasant algorithm spiral around my castle at beginning fill land in
		 * areas (fill, not random) random otherwise
		 */
		P: for (IPeasant p : this.getPeasants()) {
			// p.getDirectionTo(0,0);

			// initial direction around castle
			int dir = rand();
			try {
				dir = p.getDirectionTo(startx, starty)-1;
			} catch (NullPointerException e) {
			}
			for (int d = dir, c = 0; c < 6; c++, d--) {
				d = range(d);
				Point pt = World.getPositionAfterMove(p.getX(), p.getY(), d);

				// off-screen=next direction
				if (pt == null)
					continue;

				// does not own this square of land
				IRuler ruler = World.getLandOwner(pt.x, pt.y);
				if (ruler == null || !ruler.equals(this)) {

					// someone is there
					IObject obj = World.getObjectAt(pt.x, pt.y);
					if (obj != null) {

						// dodge knights and ignore peasants
						if (obj instanceof IKnight) {
							p.move(range(d - 4));
							continue P;
						} else
							continue;
					}

					// no one is there-take it
					else {
						p.move(d);
						continue P;
					}
				}
			}

			// randomly move if all else fails
			p.move(rand());
		}

	/*
	 * Knight algorithm
	 */
	K: for (IKnight k : this.getKnights()) {

		// default direction: away from centre to defend castles if they get
		// captured
		int dir = rand();
		int dist = Integer.MAX_VALUE;
		int x = k.getX();
		int y = k.getY();

		// find the closest of these castles to move to
		if (dist == Integer.MAX_VALUE) {
			for (ICastle c : World.getOtherCastles())
				// topruler.getCastles()
				if (c.getDistanceTo(x, y) < dist) {
					dist = c.getDistanceTo(x, y);
					dir = k.getDirectionTo(c.getX(), c.getY());
				}
		}

		// find closest distance to an enemy's knights
		if (dist == Integer.MAX_VALUE) {
			for (IKnight enemy : World.getOtherKnights())
				// topruler.getKnights())
				if (enemy.getDistanceTo(x, y) < dist) {
					dist = enemy.getDistanceTo(x, y);
					dir = getDirectionTo(k, new Point(enemy.getX(), enemy.getY()));
				}
		}

		// find closest distance to an enemy's peasants
		if (dist == Integer.MAX_VALUE) {
			for (IPeasant enemy : World.getOtherPeasants())
				// topruler.getPeasants()){
				if (enemy.getDistanceTo(x, y) < dist) {
					dist = enemy.getDistanceTo(x, y);
					try {
						dir = k.getDirectionTo(enemy.getX(), enemy.getY());
					} catch (NullPointerException e) {
					}
				}
			onlyPeasants = true;
		}
		if (dist == Integer.MAX_VALUE)
			nothin = true;

		// }

		// Enemy Peasant/Castle/Knight is there=capture
		// look on the bright side of death
		for (int d = dir, c = 0; c < 6; c++, d--) {
			d = range(d);
			Point pt = World.getPositionAfterMove(x, y, d);
			if (pt != null) {
				IObject obj = World.getObjectAt(pt.x, pt.y);
				if (obj != null
						&& (obj.getRuler() == null || !obj.getRuler()
								.equals(this))) {

					// dodge knights (more powerful)
					if (obj instanceof IKnight) {
						k.capture(d);
						continue;
					}
				} else if (!onlyPeasants && obj != null
						&& obj instanceof ICastle){// && (d % 2 == 0 || d == 1)){
					k.move(0);
					continue;// K;
				}
			}
		}

		// capture other
		for (int d = dir, c = 0; c < 6; c++, d--) {
			d = range(d);
			Point pt = World.getPositionAfterMove(x, y, d);
			if (pt != null) {
				IObject obj = World.getObjectAt(pt.x, pt.y);
				if (obj != null
						&& (obj.getRuler() == null || !obj.getRuler()
								.equals(this))) {
					k.capture(d);
					continue K;
				}
			}
		}

		// moving
		if (nothin && World.getLandOwner(x, y) != null
				&& World.getLandOwner(x, y).equals(this)) {
			// determined by dice roll.
			k.move(0);
			continue K;
		}
		for (int d = dir, c = 0; c < 6; c++, d--) {
			d = range(d);
			Point pt = World.getPositionAfterMove(x, y, d);

			// off screen=next dir
			if (pt == null)
				continue;

			// no one is there=move
			IObject obj = World.getObjectAt(pt.x, pt.y);
			if (obj == null) {
				if (!nothin) {
					k.move(d);
					continue K;
				}
				IRuler ruler = World.getLandOwner(pt.x, pt.y);
				if (ruler != null && ruler.equals(this)) {
					k.move(d);
					continue K;
				}
			}
		}

		// default move if all else fails (only if I have all the castles-a
		// good sign)
		k.move(dir);
	}

		/*
		 * Castle algorithm maintain a 3:1 ratio of k:p
		 */
		if (/*onlyPeasants
				|| */this.getKnights().length < this.getPeasants().length * 3)
			for (ICastle c : this.getCastles()){
				c.createKnights();
//				for(IKnight k:World.getOtherKnights()){
//					if(c.getDistanceTo(k.getX(), k.getY()) < 10){
//						
//					}
//				}
			}
		else
			for (ICastle c : this.getCastles())
				c.createPeasants();
	}

	/**
	 * random direction
	 * and now for something completely different...
	 * 
	 * @return an int from 1 to 6 inclusive
	 */
	private int rand() {
		return (int) (Math.random() * 6) + 1;
	}

	protected static int signum(int x) {
		return (x > 0) ? 1 : (x < 0) ? -1 : 0;
	}
	public static int getWorldDistance(Point src, Point dest) {
		int dx = dest.x - src.x, dy = dest.y - src.y;
		if (signum(dx) != signum(dy))
			return Math.max(Math.abs(dx), Math.abs(dy));
		else
			return Math.abs(dx + dy);
	}

	public static Point getPositionAfterMove(Point src, int dir) {
		Point p = new Point();
		switch (dir) {
		case 0:
			p.move(src.x, src.y);
			break;
		case 1:
			p.move(src.x - 1, src.y + 1);
			break;
		case 2:
			p.move(src.x, src.y + 1);
			break;
		case 3:
			p.move(src.x + 1, src.y);
			break;
		case 4:
			p.move(src.x + 1, src.y - 1);
			break;
		case 5:
			p.move(src.x, src.y - 1);
			break;
		case 6:
			p.move(src.x - 1, src.y);
			break;
		default:
			return null;
		}
		if (getWorldDistance(new Point(0,0), p) == 36) {
			return null;
		} else
			return p;
	}
	public static int getWorldDirection(Point src, Point dest) {
		Point temp = getPositionAfterMove(src, 0);
		if (temp == null)
			return -1;
		int minDist = getWorldDistance(temp, dest);
		int dir = 0;
		for (int i = 1; i < 7; i++) {
			temp = getPositionAfterMove(src, i);
			if (temp == null)
				continue;
			int tempDist = getWorldDistance(temp, dest);
			if (tempDist < minDist) {
				minDist = tempDist;
				dir = i;
			}
		}
		return dir;
	}

	public static int getDirectionTo(IObject o, Point dest){
		return getWorldDirection(new Point(o.getX(), o.getY()), dest);
	}

}