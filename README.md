CodeRuler
=========

Along with Kevin Choi, this code won Richmond Hill High School 3rd place in IBM's CASCON Competition in 2010.

Our code is in ./src/MyRuler.java and ./MyRuler2.java. We do not own any other code, however, it is publicly available. Included for reference / interest.
